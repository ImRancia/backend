module.exports = (sequelize, Sequelize) => {
    const Visiteur = sequelize.define("visiteur", {
      NumVisiteur: {
        type: Sequelize.STRING
      },
      nom: {
        type: Sequelize.STRING
      },
      adresse: {
        type: Sequelize.STRING
      }
    });
  
    return Visiteur;
  };
  