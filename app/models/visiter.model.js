module.exports = (sequelize, Sequelize) => {
    const Visiter = sequelize.define("visiter", {
      NumVisiteur: {
        type: Sequelize.STRING
      },
      NumSite: {
        type: Sequelize.STRING
      },
      NbJours: {
        type: Sequelize.STRING
      },
      DateVisite: {
        type: Sequelize.STRING
      }

    });
  
    return Visiter;
  };
  