module.exports = (sequelize, Sequelize) => {
    const Site = sequelize.define("site", {
      NumSite: {
        type: Sequelize.STRING
      },
      nom: {
        type: Sequelize.STRING
      },
      lieu: {
        type: Sequelize.STRING
      },
      TarifJournalier: {
        type: Sequelize.STRING
      }
     
    });
  
    return Site;
  };
  