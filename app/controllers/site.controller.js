const db = require("../models");
const Site = db.sites;
const Op = db.Sequelize.Op;

// Create and Save a new Site
exports.create = (req, res) => {
  // Validate request
  if (!req.body.NumSite) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Site
  const site = {
    NumSite: req.body.NumSite,
    nom: req.body.nom,
    lieu: req.body.lieu,
    TarifJournalier: req.body.TarifJournalier,
  };

  // Save Site in the database
  Site.create(site)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Site."
      });
    });
};

// Retrieve all Sites from the database.
exports.findAll = (req, res) => {
  const NumSite = req.query.NumSite;
  var condition = NumSite ? { NumSite: { [Op.like]: `%${NumSite}%` } } : null;

  Site.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving site."
      });
    });
};

// Find a single Site with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Site.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Site with id=" + id
      });
    });
};

// Update a Site by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Site.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Site was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Site with id=${id}. Maybe Site was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Site with id=" + id
      });
    });
};

// Delete a Site with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Site.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Site was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Site with id=${id}. Maybe Site was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Site with id=" + id
      });
    });
};

// Delete all Sites from the database.
exports.deleteAll = (req, res) => {
  Site.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Sites were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all site."
      });
    });
};

