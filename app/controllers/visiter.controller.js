const db = require("../models");
const Visiter = db.visiters;
const Op = db.Sequelize.Op;

// Create and Save a new Visiter
exports.create = (req, res) => {
  // Validate request
  if (!req.body.NumVisiteur) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Visiter
  const visiter = {
    NumVisiteur: req.body.NumVisiteur,
    NumSite: req.body.NumSite,
    NbJours: req.body.NbJours,
    DateVisite: req.body.DateVisite,
   // published: req.body.published ? req.body.published : false
  };

  // Save Visiter in the database
  Visiter.create(visiter)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Visiter."
      });
    });
};

// Retrieve all Visiters from the database.
exports.findAll = (req, res) => {
  const NbJours = req.query.NbJours;
  var condition = NbJours ? { NbJours: { [Op.like]: `%${NbJours}%` } } : null;

  Visiter.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving visiter."
      });
    });
};

// Find a single Visiter with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Visiter.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Visiter with id=" + id
      });
    });
};

// Update a Visiter by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Visiter.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Visiter was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Visiter with id=${id}. Maybe Visiter was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Visiter with id=" + id
      });
    });
};

// Delete a Visiter with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Visiter.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Visiter was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Visiter with id=${id}. Maybe Visiter was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Visiter with id=" + id
      });
    });
};

// Delete all Visiters from the database.
exports.deleteAll = (req, res) => {
  Visiter.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Visiters were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all visiter."
      });
    });
};


