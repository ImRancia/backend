const db = require("../models");
const Visiteur = db.visiteurs;
const Op = db.Sequelize.Op;

// Create and Save a new Visiteur
exports.create = (req, res) => {
  // Validate request
  if (!req.body.NumVisiteur) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Visiteur
  const visiteur = {
    NumVisiteur: req.body.NumVisiteur,
    nom: req.body.nom,
    adresse: req.body.adresse,
    //published: req.body.published ? req.body.published : false
  };

  // Save Visiteur in the database
  Visiteur.create(visiteur)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Visiteur."
      });
    });
};

// Retrieve all Visiteurs from the database.
exports.findAll = (req, res) => {
  const NumVisiteur = req.query.NumVisiteur;
  var condition = NumVisiteur ? { NumVisiteur: { [Op.like]: `%${NumVisiteur}%` } } : null;

  Visiteur.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving visiteur."
      });
    });
};

// Find a single Visiteur with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Visiteur.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Visiteur with id=" + id
      });
    });
};

// Update a Visiteur by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Visiteur.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Visiteur was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Visiteur with id=${id}. Maybe Visiteur was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Visiteur with id=" + id
      });
    });
};

// Delete a Visiteur with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Visiteur.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Visiteur was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Visiteur with id=${id}. Maybe Visiteur was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Visiteur with id=" + id
      });
    });
};

// Delete all Visiteurs from the database.
exports.deleteAll = (req, res) => {
  Visiteur.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Visiteurs were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all visiteur."
      });
    });
};



