module.exports = app => {
    const sites = require("../controllers/site.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", sites.create);
  
    // Retrieve all Tutorials
    router.get("/", sites.findAll);
  
    // Retrieve all published Tutorials
 
  
    // Retrieve a single Tutorial with id
    router.get("/:id", sites.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", sites.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", sites.delete);
  
    // Delete all Tutorials
    router.delete("/", sites.deleteAll);
  
    app.use('/api/sites', router);
  };
  