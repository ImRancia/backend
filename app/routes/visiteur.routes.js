module.exports = app => {
    const visiteurs = require("../controllers/visiteur.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", visiteurs.create);
  
    // Retrieve all Tutorials
    router.get("/", visiteurs.findAll);
  
    // Retrieve all published Tutorials
   
  
    // Retrieve a single Tutorial with id
    router.get("/:id", visiteurs.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", visiteurs.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", visiteurs.delete);
  
    // Delete all Tutorials
    router.delete("/", visiteurs.deleteAll);
  
    app.use('/api/visiteurs', router);
  };
  