module.exports = app => {
    const visiters = require("../controllers/visiter.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", visiters.create);
  
    // Retrieve all Tutorials
    router.get("/", visiters.findAll);
  
    // Retrieve all published Tutorials
    
  
    // Retrieve a single Tutorial with id
    router.get("/:id", visiters.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", visiters.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", visiters.delete);
  
    // Delete all Tutorials
    router.delete("/", visiters.deleteAll);
  
    app.use('/api/visiters', router);
  };
  